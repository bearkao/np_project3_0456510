#include <windows.h>
#include <list>
#include <string.h>
#include <stdio.h>
#include <io.h>
// #include <Winsock2.h>

using namespace std;

#include "resource.h"

#define SERVER_PORT 8080
#define MAXREAD 10000
#define F_CONNECTING 0 
#define F_READING 1 
#define F_WRITING 2 
#define F_DONE 3

#define WM_SOCKET_NOTIFY (WM_USER + 1)

const char html_head[] = "<html> \
        <head> \
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" /> \
        <title>Network Programming Homework 3</title> \
        </head> \
        <body bgcolor=#336699> \
        <font face=\"Courier New\" size=2 color=#FFFF99> \
        <table width=\"800\" border=\"1\"><tr>";

BOOL CALLBACK MainDlgProc(HWND, UINT, WPARAM, LPARAM);
int EditPrintf (HWND, TCHAR *, ...);
//=================================================================
//	Global Variables
//=================================================================
list<SOCKET> Socks;
SOCKET g_ssock;
int g_conn=0;
int cur_conn;
struct sockaddr_in client_sin[5];
SOCKET client_fd[5];
int statusA[5] = {F_READING};
FILE* files_fd[5];
int live_client[5] = {1};

int process_read(char* read_buf, int n, int col, int ssock) {
	int i, read_over=0;
	char buf[MAXREAD];
	memset(buf, '\0', sizeof(buf));
	for(i=0; read_buf[i]!='\0'; i++) {
		if(read_buf[i]=='\n') {
			sprintf(buf, "<script>document.all['m%d'].innerHTML += \"<br>\";</script>",col);
			send(ssock, buf, (int)strlen(buf), 0);
		}
		else {
			sprintf(buf, "<script>document.all['m%d'].innerHTML += \"%c\";</script>", col, read_buf[i]);
			send(ssock, buf, (int)strlen(buf), 0);
		}
		if(read_buf[i] == '%') {
			read_over = 1;			
		}
	}	
	return read_over;
}

void initial_nonblock_socket(struct sockaddr_in* client_sin, SOCKET* client_fd, char* ser_addr, int p ) {
	*client_fd = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);

	client_sin->sin_family = AF_INET;
    client_sin->sin_addr.s_addr = inet_addr(ser_addr);
    client_sin->sin_port = htons(p);

	connect(*client_fd, (SOCKADDR*)client_sin, sizeof(*client_sin) );
}

char* find_assign(char* query_head) {
	char* w;
	for(w=query_head; *w!='\0'; w++) {
		if(*w=='=')
			return w+1;
	}
	return w;
}

char* parse_query(char* query) {
	char* w = query;
	while((*w!='&') && (*w!='\0')) {
		w++;
	}
	if(*w=='&') {
		*w = '\0';
		return w+1;
	}
	else {
		*w = '\0';
		return NULL;
	}
}

/* return 1: get method, return 0: post method, -1: unknown method */
/* f_name: file name (eg. form_get.html) */
/* type: cgi/html/NULL */
/* cgi_arg: [0]NULL or eg. h1=127.0.0.1&p1=8081&f1=gg&h2=&p2=&f2=&h3=&p3=&f3=&h4=&p4=&f4=&h5=&p5=&f5= */
int process_request(char* f_name, char* type, char* cgi_arg, int ssock, HWND hwndEdit) {

	char buf[MAXREAD];
	char buf2[MAXREAD];
	memset(buf, '\0', sizeof(buf));	
	memset(buf2, '\0', sizeof(buf2));	

	int return_val = -1, i, c, len, gap, f_start;
	int prev_i=0;
	char nl[8];
	// char* pch = buf; 

	sprintf(nl, "%s", "<br>");

	if(recv(ssock, buf, MAXREAD, 0)>0) {
		// EditPrintf(hwndEdit, TEXT("buf: %s\r\n"), buf);
		for(i=0; buf[i]!='\0'; i++){
			if(buf[i]=='\n') {
				//send( ssock, buf+prev_i, i-prev_i+1, 0 ); // include '\n'
				// send( ssock, nl, (int)strlen(nl), 0 );
				buf[i] = '\0';
				len = strlen(buf);
				prev_i = i+1;
				break;
			}	
		}
		if(strncmp(buf, "GET", 3)==0)
			return_val = 1;
		else if(strncmp(buf, "POST", 4)==0)
			return_val = 0;
		gap = len;
		sprintf(buf2, "len: %d<br>", len);
		send( ssock, buf2, (int)strlen(buf2), 0 );
	}
	else {
		return -1;
	}

	/* find request file, gap */
	/* sample get method: GET /~bear/form_get.html HTTP/1.1 */
	/* so find the last two '/' */
	for(i=len-1, c=0; i>=0; i--) {
		if(buf[i]==' ')
			gap = i;
		if(buf[i]=='/')
			c++;
		if(c==2)
			break;
	}
	f_start = i;

	/* find file type */
	int dot=-1, f_gap;
	for(f_gap=gap, i=f_start; i<gap; i++) {
		// sprintf(buf2, "buf[i]: %c i: %d<br>", buf[i], i);
		// send( ssock, buf2, (int)strlen(buf2), 0 );
		// EditPrintf(hwndEdit, TEXT("i: %d\r\n"), i);
		if(buf[i]=='?') {
			f_gap = i;	
			break;
		}
		if(buf[i]=='.')
			dot = i;
	}

	/* assign */
	strncpy_s(f_name, 16, buf+f_start+1, f_gap-f_start-1);
	if(dot!=-1)
		strncpy_s(type, 16, buf+dot+1, f_gap-dot-1);
	else
		type[0] = '\0';
	if(f_gap!=gap)
		strncpy_s(cgi_arg, 4096, buf+f_gap+1, gap-f_gap-1);
	else
		cgi_arg[0] = '\0';

	return return_val;
}


void print_result(int code, char* f_name, char* type, char* cgi_arg, int ssock) {
	char buf[MAXREAD];
	sprintf(buf, "code: %d<br>", code);
	send( ssock, buf, (int)strlen(buf), 0 );
	sprintf(buf, "f_name: %s<br>", f_name);
	send( ssock, buf, (int)strlen(buf), 0 );	
	if(type[0]!='\0') {
		sprintf(buf, "type: %s<br>", type);
		send( ssock, buf, (int)strlen(buf), 0 );
	}
	if(cgi_arg[0]!='\0') {
		sprintf(buf, "cgi_arg: %s<br>", cgi_arg);
		send( ssock, buf, (int)strlen(buf), 0 );
	}	
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	
	return DialogBox(hInstance, MAKEINTRESOURCE(ID_MAIN), NULL, MainDlgProc);
}

BOOL CALLBACK MainDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	WSADATA wsaData;

	static HWND hwndEdit;
	static SOCKET msock, ssock;
	static struct sockaddr_in sa;
	char type[16], cgi_arg[4096], f_name[16];
	char buf[MAXREAD];
	char buf2[MAXREAD];
	char* query_var;
	int code;
	int err;
	int n,i, NeedWrite;
	int conn = 0; /* number of connection */
	char*	hosts[5],
			*files[5];
	int ports[5];

	switch(Message) 
	{
		case WM_INITDIALOG:
			hwndEdit = GetDlgItem(hwnd, IDC_RESULT);
			break;
		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case ID_LISTEN:
					// service start here
					WSAStartup(MAKEWORD(2, 0), &wsaData);

					//create master socket
					msock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

					if( msock == INVALID_SOCKET ) {
						EditPrintf(hwndEdit, TEXT("=== Error: create socket error ===\r\n"));
						WSACleanup();
						return TRUE;
					}

					err = WSAAsyncSelect(msock, hwnd, WM_SOCKET_NOTIFY, FD_ACCEPT | FD_CLOSE | FD_READ | FD_WRITE);

					if ( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
						closesocket(msock);
						WSACleanup();
						return TRUE;
					}

					//fill the address info about server
					sa.sin_family		= AF_INET;
					sa.sin_port			= htons(SERVER_PORT);
					sa.sin_addr.s_addr	= INADDR_ANY;

					//bind socket
					err = bind(msock, (LPSOCKADDR)&sa, sizeof(struct sockaddr));

					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: binding error ===\r\n"));
						WSACleanup();
						return FALSE;
					}

					err = listen(msock, 2);
		
					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: listen error ===\r\n"));
						WSACleanup();
						return FALSE;
					}
					else {
						EditPrintf(hwndEdit, TEXT("=== Server START ===\r\n"));
					}

					break;
				case ID_EXIT:
					EndDialog(hwnd, 0);
					break;
			};
			break;

		case WM_CLOSE:
			EndDialog(hwnd, 0);
			break;

		case WM_SOCKET_NOTIFY:
			switch( WSAGETSELECTEVENT(lParam) )
			{
				case FD_ACCEPT:
					ssock = accept(msock, NULL, NULL);
					Socks.push_back(ssock);
					EditPrintf(hwndEdit, TEXT("=== Accept one new client(%d), List size:%d ===\r\n"), ssock, Socks.size());

					memset(cgi_arg, '\0', sizeof(cgi_arg));
					memset(f_name, '\0', sizeof(f_name));
					memset(type, '\0', sizeof(type));
					// memset(query, '\0', sizeof(query));
					sprintf(buf, "%s", "HTTP/1.1 200 OK\n");
					send( ssock, buf, (int)strlen(buf), 0 );

					code = process_request(f_name, type, cgi_arg, ssock, hwndEdit);
					if(code==1) {
						if(type[0]!='\0') {
							if(strcmp(type, "cgi")==0) {
								query_var = cgi_arg;
								int i,j;
								for(i=0; i<5; i++) {
									hosts[i] = find_assign(query_var);
									query_var = parse_query(query_var);
									if(hosts[i][0]=='\0') {
										continue;
									}
									char* w = find_assign(query_var);
									query_var = parse_query(query_var);
									ports[i] = atoi(w);
									files[i] = find_assign(query_var);
									query_var = parse_query(query_var);
									conn++;
								}						
								for(i=0; i<conn; i++) {
									initial_nonblock_socket(
										&client_sin[i], 
										&client_fd[i], 
										hosts[i],
										ports[i]
									);
									files_fd[i] = fopen(files[i],"r");
								}								
								sprintf(buf, "%s", "Content-Type: text/html\n\n");
								send( ssock, buf, (int)strlen(buf), 0 );
								send( ssock, html_head, (int)strlen(html_head), 0 );
								for(j=0; j<5; j++) {
									if(hosts[j][0]!='\0') {
										sprintf(buf, "<td>%s:%d:%s</td>", hosts[j], ports[j], files[j]);
										send( ssock, buf, (int)strlen(buf), 0 );
									}
								}
								sprintf(buf, "%s","</tr><tr>");
								send( ssock, buf, (int)strlen(buf), 0 );
								for(j=0; j<5; j++) {
									if(hosts[j][0]!='\0') {
										sprintf(buf, "<td valign=\"top\" id=\"m%d\"></td>", j);
										send( ssock, buf, (int)strlen(buf), 0 );
									}
								}
								sprintf(buf, "%s", "</tr></table>");
								send( ssock, buf, (int)strlen(buf), 0 );
								for(i=0; i<conn; i++) {
									WSAAsyncSelect(client_fd[i], hwnd, WM_SOCKET_NOTIFY, FD_CLOSE | FD_READ | FD_WRITE);
									live_client[i] = 1;
									statusA[i] = F_READING;
								}
								EditPrintf(hwndEdit, TEXT("=== test... conn: %d ===\r\n"), conn);
								g_conn = conn;
								cur_conn = g_conn;
								g_ssock = ssock;
							} 	
							else if(strcmp(type, "html")==0) {
								sprintf(buf, "%s", "Content-Type: text/html\n\n");
								send( ssock, buf, (int)strlen(buf), 0 );
								FILE* f = fopen(f_name,"r");
								char html_buf[MAXREAD];
								while(fgets(html_buf, MAXREAD, f) != NULL) {
									html_buf[strlen(html_buf)-1] = '\0';
									sprintf(buf, "%s", html_buf);
									send( ssock, buf, (int)strlen(buf), 0 );
								}
							}
						}						
					}
						// print_result(code, f_name, type, cgi_arg, ssock);
					EditPrintf(hwndEdit, TEXT("=== Finish... ===\r\n"));
					break;
				case FD_READ:
				//Write your code for read event here.
					for(i=0; i<g_conn; i++) {
						if(live_client[i]==1)
							WSAAsyncSelect(client_fd[i], hwnd, WM_SOCKET_NOTIFY, FD_CLOSE | FD_READ | FD_WRITE);
					}				
					for(i=0; i<g_conn; i++) {
						if(statusA[i]==F_READING) {
							memset(buf, '\0', sizeof(buf));
							n = recv(client_fd[i], buf, MAXREAD, 0);
							if (n <= 0) {
								if(n==0) {
									/* exit */
									statusA[i] = F_DONE;
								}
							}
							else {
								if(process_read(buf, n, i, g_ssock)) {
									statusA[i] = F_WRITING;
								}
							}
						}
					}
					break;
				case FD_WRITE:
					for(i=0; i<g_conn; i++) {
						if(live_client[i]==1)
							WSAAsyncSelect(client_fd[i], hwnd, WM_SOCKET_NOTIFY, FD_CLOSE | FD_READ | FD_WRITE);
					}					
				//Write your code for write event here
					for(i=0; i<g_conn; i++) {
						if(statusA[i]==F_WRITING) {
							memset(buf, '\0', sizeof(buf));
							memset(buf2, '\0', sizeof(buf2));
							if(fgets(buf, MAXREAD, files_fd[i]) != NULL) {
								NeedWrite = strlen(buf);
								buf[NeedWrite-1] = '\0';
								sprintf(buf2, "<script>document.all['m%d'].innerHTML += \"%s<br>\";</script>", i, buf);
								send(g_ssock, buf2, (int)strlen(buf2), 0);
								buf[NeedWrite-1] = '\n'; // restore '\n' so command can send to shell
								n = send(client_fd[i], buf, NeedWrite, 0);
								NeedWrite -= n;
								if (n <= 0 || NeedWrite <= 0) { // write finished
									/* assume write entire command, not separately */					
									/* otherwise, shell will get wrong command */
									statusA[i] = F_READING;
								} 
								else {
									sprintf(buf, "<script>document.all['m%d'].innerHTML += \"Ooops, separately write!!<br>\";</script>",i);
									send(g_ssock, buf, (int)strlen(buf), 0);									
								}
								if(strcmp(buf,"exit\n")==0) {
									EditPrintf(hwndEdit, TEXT("=== client exit... ===\r\n"));
									live_client[i] = 0;
									cur_conn--;									
								}
							}
							else { // maybe eof
								EditPrintf(hwndEdit, TEXT("=== end... ===\r\n"));
								sprintf(buf2, "<script>document.all['m%d'].innerHTML += \"eof!!<br>\";</script>",i);
								send(g_ssock, buf2, (int)strlen(buf2), 0);
								close(client_fd[i]);
								fclose(files_fd[i]);
							}
						}
					}
					break;
				case FD_CLOSE:
					EditPrintf(hwndEdit, TEXT("=== close... ===\r\n"));				
					break;
			};
			break;
		
		default:
			return FALSE;


	};

	return TRUE;
}

int EditPrintf (HWND hwndEdit, TCHAR * szFormat, ...)
{
     TCHAR   szBuffer [1024] ;
     va_list pArgList ;

     va_start (pArgList, szFormat) ;
     wvsprintf (szBuffer, szFormat, pArgList) ;
     va_end (pArgList) ;

     SendMessage (hwndEdit, EM_SETSEL, (WPARAM) -1, (LPARAM) -1) ;
     SendMessage (hwndEdit, EM_REPLACESEL, FALSE, (LPARAM) szBuffer) ;
     SendMessage (hwndEdit, EM_SCROLLCARET, 0, 0) ;
	 return SendMessage(hwndEdit, EM_GETLINECOUNT, 0, 0); 
}
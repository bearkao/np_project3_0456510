#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <signal.h> 

#define MAXREAD 10000
#define LISTENQ 1024

void sig_chld(int signo) {
	pid_t pid;
	int stat;
	pid = wait(&stat);
	printf("child %d terminated\n", pid);
}        

void initial_TCP_socket(struct sockaddr_in* servaddr, int* sock_fd, int p) {
	*sock_fd = socket(AF_INET,SOCK_STREAM,0);
	bzero(servaddr, sizeof(struct sockaddr_in));
	servaddr->sin_family = AF_INET;
	servaddr->sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr->sin_port = htons(p);
	if(bind(*sock_fd,
		(struct sockaddr *)servaddr, sizeof(*servaddr))<0){
		perror("bind error");
		exit(EXIT_FAILURE);
	}	
	if(listen(*sock_fd,LISTENQ)<0) {
		perror("listen error\n");
		exit(EXIT_FAILURE);
	}
}

/* return 1: get method, return 0: post method, -1: unknown method */
/* f_name: file name (eg. form_get.html) */
/* type: cgi/html/NULL */
/* cgi_arg: [0]NULL or eg. h1=127.0.0.1&p1=8081&f1=gg&h2=&p2=&f2=&h3=&p3=&f3=&h4=&p4=&f4=&h5=&p5=&f5= */
int process_request(char* f_name, char* type, char* cgi_arg) {

	char buf[MAXREAD];

	int return_val = -1, i, c, len, gap, f_start;
	char* pch = buf; 
	if(fgets(buf, sizeof(buf), stdin)!=NULL) {
		len = strlen(buf);
		buf[len-1] = '\0';
		if(strncmp(buf, "GET", 3)==0)		
			return_val = 1;
		else if(strncmp(buf, "POST", 4)==0)
			return_val = 0;
		len -= 1;
		gap = len;
	}

	/* find request file, gap */
	/* sample get method: GET /~bear/form_get.html HTTP/1.1 */
	/* so find the last two '/' */
	for(i=len-1, c=0; i>=0; i--) {
		if(buf[i]==' ')
			gap = i;
		if(buf[i]=='/')
			c++;
		if(c==2)
			break;
	}
	f_start = i;

	/* find file type */
	int dot=-1, f_gap;
	for(f_gap=gap; i<gap; i++) {
		if(buf[i]=='.')
			dot = i;
		if(buf[i]=='?') {
			f_gap = i;	
			break;
		}
	}

	/* assign */
	strncpy(f_name, buf+f_start+1, f_gap-f_start-1);
	if(dot!=-1)
		strncpy(type, buf+dot+1, f_gap-dot-1);
	else
		type[0] = '\0';
	if(f_gap!=gap)
		strncpy(cgi_arg, buf+f_gap+1, gap-f_gap-1);
	else
		cgi_arg[0] = '\0';

	return return_val;
}

void print_result(int code, char* f_name, char* type, char* cgi_arg) {
	printf("code: %d<br>", code);
	fflush(stdout);			
	printf("f_name: %s<br>", f_name);
	fflush(stdout);			
	if(type[0]!='\0') {
		printf("type: %s<br>", type);
		fflush(stdout);
	}
	if(cgi_arg[0]!='\0') {
		printf("cgi_arg: %s<br>", cgi_arg);
		fflush(stdout);
	}	
}

int main(int argc, char const *argv[]) {

	int port8080_fd, connfd, len, code;
	struct sockaddr_in servaddr_8080;
	pid_t childpid;
	socklen_t addr_len;

	initial_TCP_socket(&servaddr_8080, &port8080_fd, 8081); // port 8080
	/* from signal table: death of a child process */	
	signal(SIGCHLD, sig_chld);	

	for(;;) {
		addr_len = (socklen_t)sizeof(servaddr_8080);
		connfd = accept(port8080_fd,(struct sockaddr *)&servaddr_8080,&addr_len);
		if ( (childpid = fork()) == 0) { /* child process */
			printf("client connected!!\n");
			printf("In child process (pid = %d)\n", getpid());
			char type[16], cgi_arg[4096], f_name[16];
			close(port8080_fd); // close listening socket 
			dup2(connfd, 0);
			dup2(connfd, 1);
			dup2(connfd, 2);

			fflush(stdout);
			memset(cgi_arg, '\0', sizeof(cgi_arg));
			memset(f_name, '\0', sizeof(f_name));
			memset(type, '\0', sizeof(type));
			code = process_request(f_name, type, cgi_arg);
			printf("HTTP/1.1 200 OK\n");
			fflush(stdout);
			if(code==1) {
				if(type[0]!='\0') {
					if(strcmp(type, "cgi")==0) {
						if(cgi_arg[0]!='\0')
							setenv("QUERY_STRING", cgi_arg, 1);
						else
							setenv("QUERY_STRING", "", 1);
						char exec_file[16];
						sprintf(exec_file, "./%s", f_name);						
						char* arg[] = {exec_file, NULL};
						if(execvp(exec_file, arg)==-1) {
							printf("exec erro: %s<br>", strerror(errno));
						}						
					} 	
					else if(strcmp(type, "html")==0) {
						printf("Content-Type: text/html\n\n");
						fflush(stdout);
						FILE* f = fopen(f_name,"r");
						char html_buf[MAXREAD];
						while(fgets(html_buf, MAXREAD, f) != NULL) {
							html_buf[strlen(html_buf)-1] = '\0';
							printf("%s", html_buf);
							fflush(stdout);
						}
					}
				}
			}
			sleep(1);
			exit(0);
		} //end child process
		close(connfd); /* parent closes connected socket */		
	}	

	return 0;
}






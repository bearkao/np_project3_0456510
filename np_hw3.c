#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>

#define F_CONNECTING 0 
#define F_READING 1 
#define F_WRITING 2 
#define F_DONE 3
#define MAXREAD 10000

const char html_head[] = "<html> \
        <head> \
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" /> \
        <title>Network Programming Homework 3</title> \
        </head> \
        <body bgcolor=#336699> \
        <font face=\"Courier New\" size=2 color=#FFFF99> \
        <table width=\"800\" border=\"1\">";

void initial_nonblock_socket(struct sockaddr_in* client_sin, int* client_fd, char* ser_addr, int p ) {
	*client_fd = socket(AF_INET,SOCK_STREAM,0);
	int flags = fcntl(*client_fd, F_GETFL, 0); 
	fcntl(*client_fd, F_SETFL, flags | O_NONBLOCK);
	bzero(client_sin, sizeof(struct sockaddr_in));
	client_sin->sin_family = AF_INET;
	client_sin->sin_addr.s_addr = inet_addr(ser_addr);
	client_sin->sin_port = htons(p);

	if(connect(*client_fd,(struct sockaddr *)client_sin,sizeof(*client_sin)) < 0) {
		if (errno != EINPROGRESS) 
			exit(-1);
		perror("conn error");
	}
}

char* find_assign(char* query_head) {
	char* w;
	for(w=query_head; *w!='\0'; w++) {
		if(*w=='=')
			return w+1;
	}
	return w;
}

char* parse_query(char* query) {
	char* w = query;
	while((*w!='&') && (*w!='\0')) {
		w++;
	}
	if(*w=='&') {
		*w = '\0';
		return w+1;
	}
	else {
		*w = '\0';
		return NULL;
	}
}

int process_read(char* read_buf, int n, int col) {
	int i, read_over=0;
	for(i=0; read_buf[i]!='\0'; i++) {
		if(read_buf[i]=='\n')
			printf("<script>document.all['m%d'].innerHTML += \"<br>\";</script>",col);
		else
			printf("<script>document.all['m%d'].innerHTML += \"%c\";</script>", col, read_buf[i]);
		if(read_buf[i] == '%') {
			read_over = 1;			
		}
		fflush(stdout);
	}	
	return read_over;
}

void strip(char* b, int len) {
	int i;
	for(i=0; i<len; i++) {
		if(b[i]=='\n')
			b[i] = '\0';
	}
}

int main(int argc, char const *argv[]) {

	struct sockaddr_in client_sin[5];
	int client_fd[5];
	int conn = 0; /* number of connection */
	char*	hosts[5],
			*files[5];
	char read_buf[MAXREAD];
	int ports[5];
	int nfds = -1;

	FILE* files_fd[5];

	fd_set rfds; /* readable file descriptors*/ 
	fd_set wfds; /* writable file descriptors*/ 
	fd_set rs; /* active file descriptors*/ 
	fd_set ws; /* active file descriptors*/

	char* query = getenv("QUERY_STRING");
	char* query_var = query;

	int i,j;
	for(i=0; i<5; i++) {
		hosts[i] = find_assign(query_var);
		query_var = parse_query(query_var);
		if(hosts[i][0]=='\0') {
			continue;
		}
		char* w = find_assign(query_var);
		query_var = parse_query(query_var);
		ports[i] = atoi(w);
		files[i] = find_assign(query_var);
		query_var = parse_query(query_var);
		conn++;
	}

	for(i=0; i<conn; i++) {
		initial_nonblock_socket(
			&client_sin[i], 
			&client_fd[i], 
			hosts[i],
			ports[i]
		);
	}

	printf("Content-Type: text/html\n\n");
	printf("%s", html_head);
	fflush(stdout);
	printf("<tr>");
	for(j=0; j<5; j++) {
		if(hosts[j][0]!='\0') {
			printf("<td>%s:%d:%s</td>", hosts[j], ports[j], files[j]);
		}
	}
	fflush(stdout);
	printf("</tr><tr>");
	for(j=0; j<5; j++) {
		if(hosts[j][0]!='\0') {
			printf("<td valign=\"top\" id=\"m%d\"></td>", j);
		}
	}	
	printf("</tr></table>");
	fflush(stdout);

	// char print_str[] = "this is a test";
	// for(i=0; i<10; i++) {
	// 	for(j=0; j<5; j++) {
	// 		if(hosts[j][0]!='\0') {
	// 			printf("<script>document.all['m%d'].innerHTML += \"%s\";</script>",
	// 				j, print_str);
	// 			fflush(stdout);
	// 			printf("<script>document.all['m%d'].innerHTML += \"<br>\";</script>",j);
	// 			fflush(stdout);
	// 			sleep(1);
	// 		}
	// 	}
	// }
	FD_ZERO(&rfds); FD_ZERO(&wfds); FD_ZERO(&rs); FD_ZERO(&ws); 
	/* open commands file, and set fd for select */
	for(i=0; i<conn; i++) {
		if(nfds<client_fd[i])
			nfds = client_fd[i];
		FD_SET(client_fd[i], &rs);
		FD_SET(client_fd[i], &ws);		
		files_fd[i] = fopen(files[i],"r");
	}
	nfds += 1;
	rfds = rs; wfds = ws;	
	int statusA[5] = {F_CONNECTING}; 
	int error, n, NeedWrite;

	while (conn > 0) {
		memcpy(&rfds, &rs, sizeof(rfds)); memcpy(&wfds, &ws, sizeof(wfds));
		if ( select(nfds, &rfds, &wfds, (fd_set*)0, (struct timeval*)0) < 0 )
			exit(-1);
		for(i=0; i<conn; i++) {
			if (statusA[i] == F_CONNECTING &&
				(FD_ISSET(client_fd[i], &rfds) || FD_ISSET(client_fd[i], &wfds))) {
				if (getsockopt(client_fd[i], SOL_SOCKET, SO_ERROR, &error, (socklen_t *)&n) < 0 || error != 0) {
					// non-blocking connect failed 
					perror("getsockopt err");
					return -1;				
				}
				statusA[i] = F_READING; 
				// FD_CLR(client_fd[i], &ws);
			}
			if (FD_ISSET(client_fd[i], &rfds)) { 
				// printf("<script>document.all['m%d'].innerHTML += \"%s<br>\";</script>",i, "test 1");
				// fflush(stdout);
				n = read(client_fd[i], read_buf, MAXREAD); /* TODO: read how many byte */
				// printf("<script>document.all['m%d'].innerHTML += \"%s<br>\";</script>",i, "test 2");
				// fflush(stdout);				
				if (n <= 0) {
					/* n == -1 ==> ewouldblock? */
					/* read can be anytime, so we souldn't clear it */
					// FD_CLR(client_fd[i], &rs); 
					// FD_SET(client_fd[i], &ws);
					// statusA[i] = F_WRITING;
					/* testing */
					// printf("<script>document.all['m%d'].innerHTML += \"%s, n:%d<br>\";</script>",i, "test writing", n);
					// fflush(stdout);
					// if(n==-1) {
					// 	printf("<script>document.all['m%d'].innerHTML += \"%s<br>\";</script>",i, strerror(errno));
					// 	fflush(stdout);						
					// }
					if(n==0) {
						/* exit */
						// printf("<script>document.all['m%d'].innerHTML += \"%s<br>\";</script>",i, "client exit.....");
						// fflush(stdout);						
						FD_CLR(client_fd[i], &rs); 
						FD_CLR(client_fd[i], &ws); 
						statusA[i] = F_DONE;
					}					
				}
				else {
					/* TODO: display read mesg in html format */
					/* TODO: replace '\n' <br> */
					// statusA[i] = F_READING;					
					// printf("<script>document.all['m%d'].innerHTML += \"testread bytes: %d<br>\";</script>",i,n);
					// fflush(stdout);
					if(process_read(read_buf, n, i)) {
						statusA[i] = F_WRITING;						
						FD_SET(client_fd[i], &ws);
					}
				}
				memset(read_buf, '\0', sizeof(read_buf));
			}			
			else if(statusA[i] == F_WRITING && FD_ISSET(client_fd[i], &wfds) ) { 
				/* only write socket when read is completed */
				// printf("<script>document.all['m%d'].innerHTML += \"%s<br>\";</script>",i, "test writing in...");
				// fflush(stdout);
				sleep(1);
				/* TODO: read one line in file, send to socket, and write to web */
				if(fgets(read_buf, MAXREAD, files_fd[i]) != NULL) {
					NeedWrite = strlen(read_buf);
					strip(read_buf, NeedWrite); // strip '\n'
					printf("<script>document.all['m%d'].innerHTML += \"%s<br>\";</script>", i, read_buf);
					fflush(stdout);
					read_buf[NeedWrite-1] = '\n'; // restore '\n' so command can send to shell
					n = write(client_fd[i], read_buf, NeedWrite); NeedWrite -= n;
					if (n <= 0 || NeedWrite <= 0) { // write finished
						/* assume write entire command, not separately */					
						/* otherwise, shell will get wrong command */
						FD_CLR(client_fd[i], &ws);
						statusA[i] = F_READING;
					} 
					else {
						printf("<script>document.all['m%d'].innerHTML += \"Ooops, separately write!!<br>\";</script>",i);
						fflush(stdout);
					}					
				}
				else { // maybe eof
					// printf("<script>document.all['m%d'].innerHTML += \"eof!!<br>\";</script>",i);
					// fflush(stdout);
					fclose(files_fd[i]);
				}
				memset(read_buf, '\0', sizeof(read_buf));				
			}
		}
	}
	return 0;
}
